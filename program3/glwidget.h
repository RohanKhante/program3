#ifndef __GLWIDGET__INCLUDE__
#define __GLWIDGET__INCLUDE__

#include <QGLWidget>
#include <QOpenGLWidget>
#include <QOpenGLFunctions_3_3_Core>
#include <QMouseEvent>
#include <tinyobjloader/tiny_obj_loader.h>
#include <glm/glm.hpp>
#include <QTimer>

#define GLM_FORCE_RADIANS

using glm::vec3;
using glm::mat4;
using glm::mat3;
using glm::vec4;
using namespace std;

class GLWidget : public QOpenGLWidget, protected QOpenGLFunctions_3_3_Core { 
    Q_OBJECT

    public:
        GLWidget(QWidget *parent=0);
        ~GLWidget();

        GLuint loadShaders(const char* vertf, const char* fragf);
    protected:
        void initializeGL();
        void resizeGL(int w, int h);
        void paintGL();

        void keyPressEvent(QKeyEvent *event);
        void keyReleaseEvent(QKeyEvent *event);
        void mousePressEvent(QMouseEvent *event);
        void mouseMoveEvent(QMouseEvent *event);


    public slots:
        void animate();

    private:

        void initializeCube();
        void renderCube();
        void renderBrick(mat4 translate);
        void renderWall(vec3 translate2, float angle, vec3 rotator);

        GLuint cubeProg;
        GLuint cubeVao;
        GLint cubeProjMatrixLoc;
        GLint cubeViewMatrixLoc;
        GLint cubeModelMatrixLoc;
        GLint brickModelMatrixLoc;
        float brickHeight;
        float brickWidth;
        float brickDepth;
        float spaceInBricks;
        int bricksInRow;
        int numberRows;
        int wallShape;

        void initializeFlatModel(const tinyobj::shape_t &shape,
                                 vector<vec3> &positions,
                                 vector<vec3> &normals);
        void initializeSmoothModel(const tinyobj::shape_t &shape,
                                   vector<vec3> &positions,
                                   vector<vec3> &normals,
                                   vector<unsigned int> &indices);
        void initializeModel(const char* filename);
        void renderModelFlat();
        void renderModelSmooth();

        int numFlatVertices;
        int numSmoothIndices;

        float viewDist;

        GLuint modelProg;
        GLuint modelFlatVao;
        GLuint modelSmoothVao;
        GLint modelProjMatrixLoc;
        GLint modelViewMatrixLoc;
        GLint modelModelMatrixLoc;

        GLint modelLightPosLoc;
        GLint modelLightColorLoc;
        GLint modelLightIntensityLoc;

        GLint modelDiffuseColorLoc;
        GLint modelAmbientColorLoc;

        void initializeGrid();
        void renderGrid();

        GLuint gridProg;
        GLuint gridVao;
        GLint gridProjMatrixLoc;
        GLint gridViewMatrixLoc;
        GLint gridModelMatrixLoc;

        mat4 projMatrix;
        mat4 viewMatrix;
        mat4 modelMatrix;

        int width;
        int height;

        glm::vec3 lastVPt;
        glm::vec3 pointOnVirtualTrackball(const glm::vec2 &pt);

        bool smooth;
        std::vector <vec3> triangleArray;


        mat4 newviewMatrix;

        // Part 1 - Add two mat4 variables for pitch and yaw.
        // Also add two float variables for the pitch and yaw angles.
        mat4 pitch;
        mat4 yaw;

        float pitchAngle;
        float yawAngle;

        // Part 2 - Add a QTimer variable for our render loop.
        //QTimer timer;

        // Part 3 - Add state variables for keeping track
        //          of which movement keys are being pressed
        bool forward;
        bool right;
        bool left;
        bool backward;
        bool up;
        bool down;
        //        - Add two vec3 variables for position and velocity.
        vec3 position;
        vec3 velocity;
        //        - Add a variable for toggling fly mode
        bool flyMode;

        glm::vec2 lastPt;
        void updateView();
        
};

#endif
