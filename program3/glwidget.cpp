#include "glwidget.h"
#include <iostream>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <QTextStream>

#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

using glm::vec2;
using glm::vec3;
using glm::mat4;
using glm::perspective;
using glm::normalize;
using glm::length;
using glm::cross;
using glm::dot;
using glm::rotate;
using glm::inverse;
using glm::value_ptr;
using glm::lookAt;

using namespace std;

GLWidget::GLWidget(QWidget *parent) : QOpenGLWidget(parent) {
    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(animate()));
    timer->start(16);
}

GLWidget::~GLWidget() {
}

void GLWidget::keyPressEvent(QKeyEvent *event) {
    switch(event->key()) {
        case Qt::Key_W:
            // forward
            forward = true;
            break;
        case Qt::Key_A:
            // left
            left = true;
            break;
        case Qt::Key_S:
            // back
            backward = true;
            break;
        case Qt::Key_D:
            // right
            right = true;
            break;
        case Qt::Key_Tab:
            // toggle fly mode
            flyMode = true;
            break;
        case Qt::Key_Shift:
            // down
            down = true;
            break;
        case Qt::Key_Space:
            // up or jump
            up = true;
            break;
        case Qt::Key_B:
            // down
            down = true;
            break;
    }
}

void GLWidget::keyReleaseEvent(QKeyEvent *event) {
    switch(event->key()) {
        case Qt::Key_W:
            // forward
            forward = false;
            break;
        case Qt::Key_A:
            // left
            left = false;
            break;
        case Qt::Key_S:
            // back
            backward = false;
            break;
        case Qt::Key_D:
            // right
            right = false;
            break;
        case Qt::Key_Tab:
            // toggle fly mode
            flyMode = false;
            break;
        case Qt::Key_Shift:
            // down
            down = false;
            break;
        case Qt::Key_Space:
            // up or jump
            up = false;
            break;
        case Qt::Key_B:
            // down
            down = false;
            break;
    }
}

void GLWidget::mousePressEvent(QMouseEvent *event) {
    vec2 pt(event->x(), event->y());
    lastPt = pt;
}

void GLWidget::mouseMoveEvent(QMouseEvent *event) {
    vec2 pt(event->x(), event->y());
    vec2 d = pt-lastPt;
    // Part 1 - use d.x and d.y to modify your pitch and yaw angles
    // before constructing pitch and yaw rotation matrices with them
    yawAngle = yawAngle + d.x/200;
    pitchAngle = glm::clamp((pitchAngle + d.y/200), -((float)M_PI/2.0f), ((float)M_PI/2.0f));

    yaw = rotate(mat4(1.0f), -yawAngle, vec3(0,1,0));

    pitch = rotate(mat4(1.0f), -pitchAngle, vec3(1,0,0));


    lastPt = pt;
}

void GLWidget::animate() {
    //mat4 translation = (position+(velocity*0.016f));
    //translation = translation*(pitch*yaw);
    makeCurrent();
    velocity = vec3(0.0f);
    vec4 rightVec = yaw[0];
    vec4 forwardVec = -yaw[2];
    vec4 upVec = yaw[1];// * pitch[1];
    //first person mode (not fly mode)
    if(flyMode == false) {
        if(forward == true) {
            velocity = velocity + vec3(forwardVec) * 0.016f;
        }
        if(right == true) {
            velocity = velocity + vec3(rightVec) * 0.016f;
        }
        if(left == true) {
            velocity = velocity + vec3(-rightVec) * 0.016f;
        }
        if(backward == true) {
            velocity = velocity + vec3(-forwardVec) * 0.016f;
        }
        if(up == true) {
            velocity = velocity + vec3(upVec) * 0.016f;
        }
        if(up == false) {
            if(position[1] > 0) {
                velocity = velocity + vec3(-upVec) * 0.016f;
            }
        }
    }
    //fly mode
    if(flyMode == true) {
        if(forward == true) {
            velocity = velocity + vec3(forwardVec) * 0.016f;
        }
        if(right == true) {
            velocity = velocity + vec3(rightVec) * 0.016f;
        }
        if(left == true) {
            velocity = velocity + vec3(-rightVec) * 0.016f;
        }
        if(backward == true) {
            velocity = velocity + vec3(-forwardVec) * 0.016f;
        }
        if(up == true) {
            velocity = velocity + vec3(upVec) * 0.016f;
        }
        if(down == true) {
            velocity = velocity + vec3(-upVec) * 0.016f;
        }
    }


    if(velocity[0] > 0.00001||velocity[1] > 0.00001||velocity[2] > 0.00001  || velocity[0] < -0.00001||velocity[1] < -0.00001||velocity[2] < -0.00001) {
        velocity = glm::normalize(velocity)*1.016f;
    }

    position = position + velocity;

    mat4 translation = glm::translate(mat4(1.0f), position);
    newviewMatrix = translation * yaw * pitch;
    newviewMatrix = inverse(newviewMatrix);

    glUseProgram(modelProg);
    glUniformMatrix4fv(modelViewMatrixLoc, 1, false, value_ptr(newviewMatrix));
    glUniformMatrix4fv(modelModelMatrixLoc, 1, false, value_ptr(modelMatrix));

    glUseProgram(gridProg);
    glUniformMatrix4fv(gridViewMatrixLoc, 1, false, value_ptr(newviewMatrix));
    glUniformMatrix4fv(gridModelMatrixLoc, 1, false, value_ptr(modelMatrix));



    update();
}

void GLWidget::initializeGrid() {
    glGenVertexArrays(1, &gridVao);
    glBindVertexArray(gridVao);

    // Create a buffer on the GPU for position data
    GLuint positionBuffer;
    glGenBuffers(1, &positionBuffer);

    vec3 pts[84];
    for(int i = -10; i <= 10; i++) {

        pts[2*(i+10)] = vec3(i, -.5f, 10);
        pts[2*(i+10)+1] = vec3(i, -.5f, -10);

        pts[2*(i+10)+42] = vec3(10,-.5f, i);
        pts[2*(i+10)+43] = vec3(-10,-.5f, i);
    }

    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(pts), pts, GL_STATIC_DRAW);

    // Load our vertex and fragment shaders into a program object
    // on the GPU
    GLuint program = loadShaders(":/grid_vert.glsl", ":/grid_frag.glsl");
    glUseProgram(program);
    gridProg = program;

    // Bind the attribute "position" (defined in our vertex shader)
    // to the currently bound buffer object, which contains our
    // position data for a single triangle. This information 
    // is stored in our vertex array object.
    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    GLint positionIndex = glGetAttribLocation(program, "position");
    glEnableVertexAttribArray(positionIndex);
    glVertexAttribPointer(positionIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    gridProjMatrixLoc = glGetUniformLocation(program, "projection");
    gridViewMatrixLoc = glGetUniformLocation(program, "view");
    gridModelMatrixLoc = glGetUniformLocation(program, "model");
}
void GLWidget::initializeCube() {
    // Create a new Vertex Array Object on the GPU which
    // saves the attribute layout of our vertices.
    glGenVertexArrays(1, &cubeVao);
    glBindVertexArray(cubeVao);

    // Create a buffer on the GPU for position data
    GLuint positionBuffer;
    glGenBuffers(1, &positionBuffer);

    GLuint colorBuffer;
    glGenBuffers(1, &colorBuffer);

    GLuint normalBuffer;
    glGenBuffers(1, &normalBuffer);

    GLuint indexBuffer;
    glGenBuffers(1, &indexBuffer);

    vec3 pts[] = {
        // top
        vec3(1,1,1),    // 0
        vec3(1,1,-1),   // 1
        vec3(-1,1,-1),  // 2
        vec3(-1,1,1),   // 3

        // bottom
        vec3(1,-1,1),   // 4
        vec3(-1,-1,1),  // 5
        vec3(-1,-1,-1), // 6
        vec3(1,-1,-1),  // 7

        // front
        vec3(1,1,1),    // 8
        vec3(-1,1,1),   // 9
        vec3(-1,-1,1),  // 10
        vec3(1,-1,1),   // 11

        // back
        vec3(-1,-1,-1), // 12
        vec3(-1,1,-1),  // 13
        vec3(1,1,-1),   // 14
        vec3(1,-1,-1),  // 15

        // right
        vec3(1,-1,1),   // 16
        vec3(1,-1,-1),  // 17
        vec3(1,1,-1),   // 18
        vec3(1,1,1),     // 19

        // left
        vec3(-1,-1,1),  // 20
        vec3(-1,1,1),   // 21
        vec3(-1,1,-1),  // 22
        vec3(-1,-1,-1) // 23

    };

    vec3 ptsNormal[] = {
        // top
        vec3(0,1,0),    // 0
        vec3(0,1,0),   // 1
        vec3(0,1,0),  // 2
        vec3(0,1,0),   // 3

        // bottom
        vec3(0,-1,0),   // 4
        vec3(0,-1,0),  // 5
        vec3(0,-1,0), // 6
        vec3(0,-1,0),  // 7

        // front
        vec3(0,0,1),    // 8
        vec3(0,0,1),   // 9
        vec3(0,0,1),  // 10
        vec3(0,0,1),   // 11

        // back
        vec3(0,0,-1), // 12
        vec3(0,0,-1),  // 13
        vec3(0,0,-1),   // 14
        vec3(0,0,-1),  // 15

        // right
        vec3(1,0,0),   // 16
        vec3(1,0,0),  // 17
        vec3(1,0,0),   // 18
        vec3(1,0,0),     // 19

        // left
        vec3(-1,0,0),  // 20
        vec3(-1,0,0),   // 21
        vec3(-1,0,0),  // 22
        vec3(-1,0,0) // 23

    };

    for(int i = 0; i < 24; i++) {
        pts[i] *= .5;
    }

    vec3 colors[] = {
        // top
        vec3(0,1,0),
        vec3(0,1,0),
        vec3(0,1,0),
        vec3(0,1,0),

        // bottom
        vec3(0,.5f,0),
        vec3(0,.5f,0),
        vec3(0,.5f,0),
        vec3(0,.5f,0),

        // front
        vec3(0,0,1),
        vec3(0,0,1),
        vec3(0,0,1),
        vec3(0,0,1),

        // back
        vec3(0,0,.5f),
        vec3(0,0,.5f),
        vec3(0,0,.5f),
        vec3(0,0,.5f),

        // right
        vec3(1,0,0),
        vec3(1,0,0),
        vec3(1,0,0),
        vec3(1,0,0),


        // left
        vec3(.5f,0,0),
        vec3(.5f,0,0),
        vec3(.5f,0,0),
        vec3(.5f,0,0)
    };

    GLuint restart = 0xFFFFFFFF;
    GLuint indices[] = {
        0,1,2,3, restart,
        4,5,6,7, restart,
        8,9,10,11, restart,
        12,13,14,15, restart,
        16,17,18,19, restart,
        20,21,22,23
    };

    // Upload the position data to the GPU, storing
    // it in the buffer we just allocated.
    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(pts), pts, GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(colors), colors, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(ptsNormal), ptsNormal, GL_STATIC_DRAW);

    // Load our vertex and fragment shaders into a program object
    // on the GPU
    GLuint program = loadShaders(":/vert.glsl", ":/frag.glsl");
    glUseProgram(program);
    cubeProg = program;

    // Bind the attribute "position" (defined in our vertex shader)
    // to the currently bound buffer object, which contains our
    // position data for a single triangle. This information
    // is stored in our vertex array object.
    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    GLint positionIndex = glGetAttribLocation(program, "position");
    glEnableVertexAttribArray(positionIndex);
    glVertexAttribPointer(positionIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
    GLint colorIndex = glGetAttribLocation(program, "color");
    glEnableVertexAttribArray(colorIndex);
    glVertexAttribPointer(colorIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
    GLint normalIndex = glGetAttribLocation(program, "normal");
    glEnableVertexAttribArray(normalIndex);
    glVertexAttribPointer(normalIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    cubeProjMatrixLoc = glGetUniformLocation(program, "projection");
    cubeViewMatrixLoc = glGetUniformLocation(program, "view");
    cubeModelMatrixLoc = glGetUniformLocation(program, "model");
    brickModelMatrixLoc = glGetUniformLocation(program, "brickModel");

}
void GLWidget::renderWall(vec3 translate2, float angle, vec3 rotator)
{
    for(int i=0; i< numberRows; i++)
    {
        for(int j=0 ;j< bricksInRow; j++)
        {
            mat4 rotateCube = glm::rotate(mat4(1.0f), angle,rotator);
            mat4 translateIndivBrick = glm::translate(rotateCube,vec3((brickHeight + spaceInBricks)*i,(brickWidth+ spaceInBricks)*j,0.0f ));
            mat4 translateWall = glm::translate(translateIndivBrick, translate2);
            renderBrick(translateWall);
        }
    }
}


void GLWidget::renderBrick(mat4 translate) {
    mat4 identity = mat4(1.0f);
    mat4 scaledCube = translate * glm::scale(identity,  vec3(brickHeight, brickWidth, brickDepth));
    glUseProgram(cubeProg);
    glUniformMatrix4fv(brickModelMatrixLoc, 1, false, value_ptr(scaledCube));
    renderCube();
}

void GLWidget::renderCube() {
    glUseProgram(cubeProg);
    glBindVertexArray(cubeVao);
    glDrawElements(GL_TRIANGLE_FAN, 29, GL_UNSIGNED_INT, 0);
}

void GLWidget::initializeFlatModel(const tinyobj::shape_t &shape,
                                 vector<vec3> &positions,
                                 vector<vec3> &normals) {
    positions.clear();
    normals.clear();

    // Part 1 
    // Make sure you understand the following loop.

    // shape.mesh.indices contains 3 indices for every triangle in the mesh
    // we can loop from 0 to number of triangles (num indices / 3)
    // to iterate over each triangle

    for(size_t tri = 0; tri < shape.mesh.indices.size() / 3; tri++) {
        // Here we're getting the indices for the current triangle
        unsigned int ind0 = shape.mesh.indices[3*tri+0];
        unsigned int ind1 = shape.mesh.indices[3*tri+1];
        unsigned int ind2 = shape.mesh.indices[3*tri+2];

        // Using those indices we can get the three points of the triangle
        vec3 p0 = vec3(shape.mesh.positions[3*ind0+0],
                       shape.mesh.positions[3*ind0+1],
                       shape.mesh.positions[3*ind0+2]);

        vec3 p1 = vec3(shape.mesh.positions[3*ind1+0],
                       shape.mesh.positions[3*ind1+1],
                       shape.mesh.positions[3*ind1+2]);

        vec3 p2 = vec3(shape.mesh.positions[3*ind2+0],
                       shape.mesh.positions[3*ind2+1],
                       shape.mesh.positions[3*ind2+2]);

        // Part 1 - TODO Calculate the normal of the triangle
        vec3 n;
        n = normalize(cross((p0-p1),(p2-p1)));

        // Push the points onto our position array
        positions.push_back(p0);
        positions.push_back(p1);
        positions.push_back(p2);

        // Push the normal onto our normal array (once for each point)
        normals.push_back(n);
        normals.push_back(n);
        normals.push_back(n);
    }
}

void GLWidget::initializeSmoothModel(const tinyobj::shape_t &shape,
                                     vector<vec3> &positions,
                                     vector<vec3> &normals,
                                     vector<unsigned int> &indices) {
    positions.clear();
    normals.clear();
    triangleArray.clear();

    // Part 2 - The following code is just a copy of initializeFlatModel
    // with the exception of the indices array. We can save space in our 
    // position buffer by constructing our primitives using unsigned 
    // integers that reference specific indices in our position buffer.
    // The provided code still duplicates the positions of our vertices,
    // but uses an indices array to specify which vertices form triangles.
    // It mimicks the behavior of glDrawArrays (but is using glDrawElements),
    // by creating an indices array that goes from 0 to 3*(number of triangles)-1. 
    // If you copy over your normal calculation from Part 1, you'll see the model 
    // is still flat shaded because we haven't smoothed the normals yet. 
    // You'll do that below.

    // Part 2a - First, optimize the code below by only adding each vertex position a single
    // time, rather than once per triangle it's used in. This is essentially just copying
    // the positions from shape.mesh.positions into positions.
    for(size_t i = 0; i < shape.mesh.positions.size(); i=i+3) {
		vec3 indivPoint = vec3(shape.mesh.positions[i], shape.mesh.positions[i+1], shape.mesh.positions[i+2]);
		positions.push_back(indivPoint);
		normals.push_back(vec3(0,0,0));
		}
    
    // Part 2b - Initialize normals array to the same size as positions and set each value to
    // (0,0,0) before iterating over each triangle.

    // Part 2c - For every triangle in the model, calculate it's normal and add it to the
    // normal that corresponds to each of the triangle's vertex positions. 
    
    // Part 2d - Populate your indices array with the index data of your mesh. This involves
    // copying over the index array from shape.mesh.indices to your own indices array.

    // Part 2e - Loop over every vertex normal and normalize it. Your normals are now
    // smoothed. 

    for(size_t tri = 0; tri < shape.mesh.indices.size() / 3; tri++) {

        // Here we're getting the indices for the current triangle
        unsigned int ind0 = shape.mesh.indices[3*tri+0];
        unsigned int ind1 = shape.mesh.indices[3*tri+1];
        unsigned int ind2 = shape.mesh.indices[3*tri+2];

        // Using those indices we can get the three points of the triangle
        vec3 p0 = vec3(shape.mesh.positions[3*ind0+0],
                       shape.mesh.positions[3*ind0+1],
                       shape.mesh.positions[3*ind0+2]);

        vec3 p1 = vec3(shape.mesh.positions[3*ind1+0],
                       shape.mesh.positions[3*ind1+1],
                       shape.mesh.positions[3*ind1+2]);

        vec3 p2 = vec3(shape.mesh.positions[3*ind2+0],
                       shape.mesh.positions[3*ind2+1],
                       shape.mesh.positions[3*ind2+2]);
                       

        // Part 1 - TODO Calculate the normal of the triangle
        vec3 n;
		n = normalize(cross((p0-p1),(p2-p1)));
        // Push the points onto our position array
        //positions.push_back(p0);
        //positions.push_back(p1);
        //positions.push_back(p2);

        // Push the normal onto our normal array (once for each point)
        //normals.push_back(n);
        //normals.push_back(n);
        //normals.push_back(n);
        normals[ind0] = normals[ind0] + n;
        normals[ind1] = normals[ind1] + n;
        normals[ind2] = normals[ind2] + n;
        //x = normalize(normals[indo])

        indices.push_back(ind0);
        indices.push_back(ind1);
        indices.push_back(ind2);
    }
}

// Initialize all the necessary OpenGL state to be able
// to render a model
void GLWidget::initializeModel(const char* filename) {
    // shapes and materials are required std::vectors that will be populated
    // by tinyobj::LoadObj, see ../include/tinyobjloader for the code
    // Look at ../include/tinyobjloader/tiny_obj_loader.h 
    // for the data structures that make up shape_t, mesh_t and material_t
    // Summary:
    //  A shape consists of a mesh and a name
    //  A mesh is what we're mostly concerned with and contains arrays
    //  for positions, normals, texture coordinates, and indices. 
    //  Normals and texture coordinates only exists if they're stored
    //  in the obj file. The models I've provided don't include normals
    //  and texture coordinates. You will be calculating the normals.
    vector<tinyobj::shape_t> shapes;
    vector<tinyobj::material_t> materials;

    QFile file(filename);
    string err = tinyobj::LoadObj(shapes, materials, file);

    if(!err.empty()) {
        cerr << err << endl;
        exit(1);
    }

    cout << "# of shapes: " << shapes.size() << endl;
    cout << "# of materials: " << materials.size() << endl;

    vector<vec3> flat_positions;
    vector<vec3> flat_normals;

// Use the first mesh in the obj file to initialize positions and normals for flat shading
    initializeFlatModel(shapes[0], flat_positions, flat_normals);

    numFlatVertices = flat_normals.size();

    vector<vec3> smooth_positions;
    vector<vec3> smooth_normals;
    vector<unsigned int> smooth_indices;

// Use the first mesh in the obj file and to initialize positions, normals and indices
// for smooth shading
    initializeSmoothModel(shapes[0], smooth_positions, smooth_normals, smooth_indices);

    numSmoothIndices = smooth_indices.size();

    // Upload all our data to buffers
    GLuint flatPositionBuffer;
    glGenBuffers(1, &flatPositionBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, flatPositionBuffer);
    glBufferData(GL_ARRAY_BUFFER, flat_positions.size()*sizeof(vec3), &flat_positions[0], GL_STATIC_DRAW);

    GLuint flatNormalBuffer;
    glGenBuffers(1, &flatNormalBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, flatNormalBuffer);
    glBufferData(GL_ARRAY_BUFFER, flat_normals.size()*sizeof(vec3), &flat_normals[0], GL_STATIC_DRAW);

    GLuint smoothPositionBuffer;
    glGenBuffers(1, &smoothPositionBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, smoothPositionBuffer);
    glBufferData(GL_ARRAY_BUFFER, smooth_positions.size()*sizeof(vec3), &smooth_positions[0], GL_STATIC_DRAW);

    GLuint smoothNormalBuffer;
    glGenBuffers(1, &smoothNormalBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, smoothNormalBuffer);
    glBufferData(GL_ARRAY_BUFFER, smooth_normals.size()*sizeof(vec3), &smooth_normals[0], GL_STATIC_DRAW);

    GLuint smoothIndexBuffer;
    glGenBuffers(1, &smoothIndexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, smoothIndexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, smooth_indices.size()*sizeof(unsigned int), &smooth_indices[0], GL_STATIC_DRAW);

    // load our shaders
    GLuint program = loadShaders(":/vert.glsl", ":/frag.glsl");
    glUseProgram(program);
    modelProg = program;

    // get some variable positions
    GLint positionIndex = glGetAttribLocation(program, "position");
    GLint normalIndex = glGetAttribLocation(program, "normal");

    // bind our buffers to vertex array objects and shader attributes
    glGenVertexArrays(1, &modelFlatVao);
    glBindVertexArray(modelFlatVao);

    glBindBuffer(GL_ARRAY_BUFFER, flatPositionBuffer);
    glEnableVertexAttribArray(positionIndex);
    glVertexAttribPointer(positionIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, flatNormalBuffer);
    glEnableVertexAttribArray(normalIndex);
    glVertexAttribPointer(normalIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glGenVertexArrays(1, &modelSmoothVao);
    glBindVertexArray(modelSmoothVao);

    glBindBuffer(GL_ARRAY_BUFFER, smoothPositionBuffer);
    glEnableVertexAttribArray(positionIndex);
    glVertexAttribPointer(positionIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, smoothNormalBuffer);
    glEnableVertexAttribArray(normalIndex);
    glVertexAttribPointer(normalIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, smoothIndexBuffer);

    modelProjMatrixLoc = glGetUniformLocation(program, "projection");
    modelViewMatrixLoc = glGetUniformLocation(program, "view");
    modelModelMatrixLoc = glGetUniformLocation(program, "model");

    modelDiffuseColorLoc = glGetUniformLocation(program, "diffuseColor");
    modelAmbientColorLoc = glGetUniformLocation(program, "ambientColor");

    modelLightPosLoc = glGetUniformLocation(program, "lightPos");
    modelLightColorLoc = glGetUniformLocation(program, "lightColor");
    modelLightIntensityLoc = glGetUniformLocation(program, "lightIntensity");

    glUniform3f(modelLightPosLoc, 0,100,0);
    glUniform3f(modelLightColorLoc, 1,1,1);
    glUniform1f(modelLightIntensityLoc, 1);

    glUniform3f(modelAmbientColorLoc, 0, 0, .2);
    glUniform3f(modelDiffuseColorLoc, .25, .8, 1);
}

void GLWidget::initializeGL() {
    initializeOpenGLFunctions();

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glPointSize(4.0f);

    glEnable(GL_DEPTH_TEST);

    initializeModel(":/models/objects.obj");

    brickHeight = 0.5;
    brickDepth = 0.5;
    brickWidth = 0.5;
    spaceInBricks = 1;
    bricksInRow = 1;
    numberRows = 1;
    wallShape = 1;

    glEnable(GL_DEPTH_TEST);
    GLuint restart = 0xFFFFFFFF;
    glPrimitiveRestartIndex(restart);
    glEnable(GL_PRIMITIVE_RESTART);

    viewDist = -120;
    //viewMatrix = lookAt(vec3(0,0,viewDist),vec3(0,0,0),vec3(0,1,0));

    modelMatrix = mat4(1.0f);
    viewMatrix = mat4(1.0f);

    position =  vec3(0.0f);
    velocity = vec3(0.0f);

    pitch = mat4(1.0f);
    yaw = mat4(1.0f);
    pitchAngle = 0;
    yawAngle = 0;

    forward = false;
    backward = false;
    right = false;
    left = false;
    up = false;
    down = false;
    flyMode = false;

    initializeCube();
    initializeGrid();

    glUseProgram(modelProg);
    glUniformMatrix4fv(modelViewMatrixLoc, 1, false, value_ptr(viewMatrix));
    glUniformMatrix4fv(modelModelMatrixLoc, 1, false, value_ptr(modelMatrix));

    glUseProgram(gridProg);
    glUniformMatrix4fv(gridViewMatrixLoc, 1, false, value_ptr(viewMatrix));
    glUniformMatrix4fv(gridModelMatrixLoc, 1, false, value_ptr(modelMatrix));
}

void GLWidget::resizeGL(int w, int h) {
    width = w;
    height = h;

    float aspect = (float)w/h;

    projMatrix = perspective(45.0f, aspect, 1.0f, 10000.0f);

    glUseProgram(modelProg);
    glUniformMatrix4fv(modelProjMatrixLoc, 1, false, value_ptr(projMatrix));

    glUseProgram(gridProg);
    glUniformMatrix4fv(gridProjMatrixLoc, 1, false, value_ptr(projMatrix));
}

void GLWidget::paintGL() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if(smooth) {
        renderModelSmooth();
    } else {
        renderModelFlat();
    }
}

void GLWidget::renderModelFlat() {
    glUseProgram(modelProg);
    glBindVertexArray(modelFlatVao);
    glDrawArrays(GL_TRIANGLES, 0, numFlatVertices);
}

void GLWidget::renderModelSmooth() {
    glUseProgram(modelProg);
    glBindVertexArray(modelSmoothVao);
    glDrawElements(GL_TRIANGLES, numSmoothIndices, GL_UNSIGNED_INT, 0);
}

void GLWidget::renderGrid() {
    glUseProgram(gridProg);
    glBindVertexArray(gridVao);
    glDrawArrays(GL_LINES, 0, 84);
}

GLuint GLWidget::loadShaders(const char* vertf, const char* fragf) {
    GLuint program = glCreateProgram();

    // read vertex shader from Qt resource file
    QFile vertFile(vertf);
    vertFile.open(QFile::ReadOnly | QFile::Text);
    QString vertString;
    QTextStream vertStream(&vertFile);
    vertString.append(vertStream.readAll());
    std::string vertSTLString = vertString.toStdString();

    const GLchar* vertSource = vertSTLString.c_str();

    GLuint vertShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertShader, 1, &vertSource, NULL);
    glCompileShader(vertShader);
    {
        GLint compiled;
        glGetShaderiv( vertShader, GL_COMPILE_STATUS, &compiled );
        if ( !compiled ) {
            GLsizei len;
            glGetShaderiv( vertShader, GL_INFO_LOG_LENGTH, &len );

            GLchar* log = new GLchar[len+1];
            glGetShaderInfoLog( vertShader, len, &len, log );
            std::cout << "Shader compilation failed: " << log << std::endl;
            delete [] log;
        }
    }
    glAttachShader(program, vertShader);

    // read fragment shader from Qt resource file
    QFile fragFile(fragf);
    fragFile.open(QFile::ReadOnly | QFile::Text);
    QString fragString;
    QTextStream fragStream(&fragFile);
    fragString.append(fragStream.readAll());
    std::string fragSTLString = fragString.toStdString();

    const GLchar* fragSource = fragSTLString.c_str();

    GLuint fragShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragShader, 1, &fragSource, NULL);
    glCompileShader(fragShader);
    {
        GLint compiled;
        glGetShaderiv( fragShader, GL_COMPILE_STATUS, &compiled );
        if ( !compiled ) {
            GLsizei len;
            glGetShaderiv( fragShader, GL_INFO_LOG_LENGTH, &len );

            GLchar* log = new GLchar[len+1];
            glGetShaderInfoLog( fragShader, len, &len, log );
            std::cerr << "Shader compilation failed: " << log << std::endl;
            delete [] log;
        }
    }
    glAttachShader(program, fragShader);

    glLinkProgram(program);

    return program;
}
